assert_eq ()
{
  echo -n "$3: "
  if [ "$1" -ne "$2" ]; then
  	echo FAIL - $4 "(assert $1 eq $2)"
	#exit 1
  else
  	echo OK -
  fi
}

assert_ne ()
{
  echo -n "$3: "
  if [ "$1" -eq "$2" ]; then
  	echo FAIL - $4 "(assert $1 ne $2)"
	#exit 1
  else
  	echo OK -
  fi
}
