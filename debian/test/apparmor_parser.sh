#!/bin/sh

PARSER=apparmor_parser
OPTS="-K -q"
ADD="-a"
REMOVE="-R"
PROFILE=/usr/lib/apparmor-utils-tests/test_profile
PROFILED=/bin/true
LOADED_PROFILES="/sys/kernel/security/apparmor/profiles"

. /usr/lib/apparmor-utils-tests/common.inc

${PARSER} ${OPTS} ${ADD} ${PROFILE}
assert_eq $? 0 "loading $PROFILE for $PROFILED" "cannot load profile"
sleep 1
grep -q ${PROFILED} ${LOADED_PROFILES}
assert_eq $? 0 "checking $PROFILED is enforced" "profile not found"
sleep 1
${PARSER} ${OPTS} ${REMOVE} ${PROFILE}
assert_eq $? 0 "removing $PROFILED profile" "cannot remove profile"
sleep 1
grep -vq ${PROFILED} ${LOADED_PROFILES}
assert_eq $? 0 "checking $PROFILED is not profiled" "profile found"
