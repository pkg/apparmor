#!/bin/sh

# Use a profile which is already loaded by default but won't be used by any
# script or the system during the tests.
# dconf-service is used, but somewhat rarely.
PROFILED=/usr/lib/dconf/dconf-service
LOADED_PROFILES="/sys/kernel/security/apparmor/profiles"

. /usr/lib/apparmor-utils-tests/common.inc

# Usually a profile goes to enforce mode by default, but the utility testing purposes we
# actually want to be sure. Leftovers from other tests can influence it.
aa-enforce ${PROFILED} > /dev/null
grep -q ${PROFILED}.*enforce ${LOADED_PROFILES}
assert_eq $? 0 "go in enforce mode" "profile not in enforce mode"

ENFORCED_COUNT_ENFORCED=`aa-status --enforced`
ENFORCED_COUNT_COMPLAIN=`aa-status --complaining`
ENFORCED_TOT=$((${ENFORCED_COUNT_ENFORCED} + ${ENFORCED_COUNT_COMPLAIN}))

aa-complain ${PROFILED} > /dev/null
grep -q ${PROFILED}.*complain ${LOADED_PROFILES}
assert_eq $? 0 "go in complain mode" "profile not in complain mode"


COMPLAINING_COUNT_ENFORCED=`aa-status --enforced`
COMPLAINING_COUNT_COMPLAIN=`aa-status --complaining`
COMPLAINING_TOT=$((${COMPLAINING_COUNT_ENFORCED} + ${COMPLAINING_COUNT_COMPLAIN}))

# check that the grand totals didn't change, but the single counter changed
assert_eq ${COMPLAINING_TOT} ${ENFORCED_TOT} \
	"gran total complain/enforce" \
	"total mismatch"
assert_ne ${COMPLAINING_COUNT_ENFORCED} ${ENFORCED_COUNT_ENFORCED} \
	"change for enforced profiles in enforce->complain" \
	"counter mismatch"
assert_ne ${COMPLAINING_COUNT_COMPLAIN} ${ENFORCED_COUNT_COMPLAIN} \
	"change in complaininig profiles in enforce->complain" \
	"counter mismatch"

aa-disable ${PROFILED} > /dev/null
grep -qv ${PROFILED} ${LOADED_PROFILES}
assert_eq $? 0 "disable profile" "profile not disabled"

DISABLED_COUNT_ENFORCED=`aa-status --enforced`
DISABLED_COUNT_COMPLAIN=`aa-status --complaining`
DISABLED_TOT=$((${DISABLED_COUNT_ENFORCED} + ${DISABLED_COUNT_COMPLAIN}))

# check that we actually removed a profile (-ne should be strong enough)
assert_ne ${DISABLED_TOT} ${ENFORCED_TOT} \
	"gran total enforce/disable" \
	"total mismatch"
# and that profiles counters changed after disabling the profile
assert_ne ${DISABLED_COUNT_ENFORCED} ${ENFORCED_COUNT_ENFORCED} \
	"change for enforced profiles in enforce->disable" \
	"counter mismatch"
assert_eq ${DISABLED_COUNT_COMPLAIN} ${ENFORCED_COUNT_COMPLAIN} \
	"change for complaining profiles in enforce->disable" \
	"counter mismatch"
